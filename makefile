CC=gcc
CFLAGS=-g3 -O3 -c -Wall -std=c99
LDFLAGS=-g
SOURCES=tests/test_unatedec.c src/core.c src/io.c src/validation.c src/stack.c src/simplex.c
LIBS=libs/liblpsolve55.a -ldl -lm
INCLUDES= -Ilp_solve_5.5 -Ilp_solve_5/bfp -Ilp_solve_5/bfp/bfp_LUSOL -Ilp_solve_5.5/colamd
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=dasg

all: $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf src/*.o tests/*.o  *.o dasg

