#include "../src/core.h"
#include "../src/validation.h"
#include "../src/io.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>


#include <string.h>

int main()
{
    clock_t begin, end;
   int i, j, n=4, m=0, entries, avg=0,  iterations=1,n_undef=0;
   output_t o_f;
   bool *input=NULL, *defined=NULL, **mat, **mat_d;
   double **mat_w, avg_gc=0, gc = 0, avg_time=0;
//   int secwait = 2;
   FILE *filelist, *log, *newfilelist;

   srand(time(NULL));

   int filenamemaxsize = 50;
   char filename[filenamemaxsize];

   filelist = fopen("data/filelist.txt", "r");
   newfilelist = fopen("data/newfilelist.txt", "w");
   
   if (filelist == NULL) {
        printf("[ERROR] Error en la apertura del listado.\n");
        exit(-1);
   }

    log = fopen("log.txt", "w");


    while (fgets(filename,filenamemaxsize,filelist)) {

        strtok(filename,"\n");
        printf("%s\n",filename);
        fprintf(log,"%s\n",filename);
        read_01(filename,&n,&input);
        
        if (n == 0) {
            //fallo al abrir el fichero
            printf("[ERROR] Fallo al abrir el fichero de entrada %s.\n", filename);
            continue;
        }
        entries = (1<<n);

        undefine(n,0,&defined,&n_undef);

        iterations = 10;
        for (i=0; i<iterations; i++) {

            begin = clock();
            unate_dec(&n,input,defined,&m,&o_f);
            printf("Iteration %d, number of solutions: %d\n",i+1,m);
            avg += m;
            mat = (bool **) calloc(m, sizeof(bool *));
            mat_d = (bool **) calloc(m, sizeof(bool *));
            mat_w = (double **) calloc(m, sizeof(double *));
            
            populate_matrices(&m,&entries,mat,mat_d,mat_w);

            end = clock();
            avg_time += (double) (end - begin) / CLOCKS_PER_SEC;

            printf("---------------------------------------------\n");
            printf("Solution test: %d\n",test_solution(&input,&defined,&n,&m,mat,mat_d,(int *)&o_f));
            printf("Weights test: %d\n",test_weights(&input,&defined,&n,&m,mat_w,(int *)&o_f));
            test_gencap(&input,&n,&n_undef,&m,mat_w,(int *) &o_f,&gc);
            printf("Generalization capability: %2.2f\n", gc);
            printf("Calculation time: %2.6f\n",(double) (end-begin)/CLOCKS_PER_SEC);
            for (int k=0; k < m; k++){
                printf("Solution number %d: \t",k);
                for (int l=0; l < entries; l++){
                    printf("%c",mat_d[k][l]?(mat[k][l]?'1':'0'):'*');
                }
                printf("\n");
            }

            avg_gc += gc;

            for (j=0; j < m; j++) {
                free(mat[j]);
                free(mat_d[j]);
                free(mat_w[j]);
            }

            m = 0;
            free(mat);
            free(mat_d);
            free(mat_w);
            mat   = NULL;
            mat_d = NULL;
            mat_w = NULL;

//            if(i<iterations-1){
//                printf("Waiting %d seconds till next iteration\n",secwait);
//                sleep(secwait);
//            }
        }

        printf("\n---------------------------------------------\n");
        printf("Average number of solutions is: %.2f\n",(double) avg/iterations);
        printf("Average generalization capability is: %.2f%%\n",(double) avg_gc/iterations);
        printf("Average calculation time is: %.6f seconds\n",(double) avg_time/iterations);
        printf("=============================================\n\n");
        //if (avg_gc/iterations < 101 && avg != 0) {
            fprintf(log,"Average number of solutions is: %.2f\n",(double) avg/iterations);
            fprintf(log,"Average generalization capability is: %.2f%%\n",(double) avg_gc/iterations);
            fprintf(log,"Average calculation time is: %.6f\n seconds",(double) avg_time/iterations);
            fprintf(log,"=============================================\n\n");
            fprintf(newfilelist,filename);
            fprintf(newfilelist,"\n");
        //}
        avg = avg_gc = avg_time = 0;


        free(input);
        free(defined);
    }

    fclose(filelist);
    fclose(log);
    fclose(newfilelist);

    exit(0);

}
