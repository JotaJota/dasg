
#ifndef io_h
#define io_h
#include <stdbool.h>

void read_file(char * file, int * inputs, bool ** input, bool ** defined, int * n_undef);
void read_01(char * file, int * inputs, bool ** input);
void read_TG(char * file, int * inputs, bool ** training, int * n_train);
void undefine(const int n_inputs, const int percentage, bool **defined, int *n_undef);

#endif
