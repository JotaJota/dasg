#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

void read_file(char * file, int * n_inputs, bool ** input, bool ** defined, int * n_undef)  {
    /*
     * Reads a file which contains a sequence of 0's,1's or ?'s without spaces
     * or \n and returns two vectors, one will be the output column of the
     * truth table of the function, and the other one will specify whether 
     * the value on the respective position of the first vector is defined or not
     */
    int fsize, c, i=0;
    FILE *f;

    f = fopen(file, "r");
    if (f) {
        fseek(f, 0L, SEEK_END);
        fsize = ftell(f);
        rewind(f);

        *input = (bool *) calloc(fsize, sizeof(bool));
        *defined = (bool *) calloc(fsize, sizeof(bool));
        *n_undef = 0;

        while ((i<fsize) && ((c = getc(f)) != EOF)) {
            if (c == '?') {
                (*input)[i] = (*defined)[i] = 0;
                (*n_undef)++;
            } else {
                (*input)[i] = c - '0';     
                (*defined)[i] = 1;
            }
            i++;
        }
        fclose(f);
    } else {
    	fprintf(stderr, "[io.c - read_file] Error reading input file.\n");
    	exit(-1);
    }
    *n_inputs = log2(i);
}

void undefine(const int n_inputs, const int percentage,  bool **defined, int *n_undef) { 
	int i = 0;
	int random_pos;
	const int n_entries = 1 << n_inputs;
	*defined = (bool *) calloc(n_entries,sizeof(bool));

	*n_undef = n_entries * percentage / 100;
	
	//calloc initialiazes all positions in mem to 0, so we set to 1 (defined) 
	//n_entries - n_undef positions, resulting in percentage positions set to 0 (undefined)
	while (i < (n_entries - *n_undef)){

		random_pos = rand() % n_entries;

		if (!(*defined)[random_pos]){
			(*defined)[random_pos] = 1;	
			i++;
		}

	}
}


    void read_01(char * file, int * n_inputs, bool ** input) {
        /*
         * Reads a file which contains a sequence of 0's or/and 1's without spaces
         * and returns a boolean vector containing this information. True for each 
         * 1 and false for each 0. Also returns the number of inputs of the given 
         * function according to the number of patterns in the file.
         */
        int fsize, c, i=0;
        FILE *f;

        f = fopen(file, "r");

        if (f) {
            fseek(f, 0L, SEEK_END);
            fsize = ftell(f);
            rewind(f);

            *input = (bool *) calloc(fsize, sizeof(bool));

            while ((i<fsize) && ((c = getc(f)) != EOF)) {
                    (*input)[i] = c - '0';     
                    i++;
            } 
            fclose(f);
            *n_inputs = log2(i);
        } else
            *n_inputs = 0;

    }

    void read_TG(char * file, int * n_inputs, bool ** training, int * n_gen) {
        /*
         * Reads a file which contains a sequence of T's or/and G's (meaning 
         * training and generalization) without spaces
         * and returns a boolean vector containing this information. True for each 
         * T and false for each G. Also returns the number of inputs of the given 
         * function according to the number of patterns in the file and the number
         * of patterns used for generalization.
     */
    int fsize, c, i=0;
    FILE *f;

    *n_gen = 0;


    f = fopen(file, "r");
    if (f) {
        fseek(f, 0L, SEEK_END);
        fsize = ftell(f);
        rewind(f);

        *training = (bool *) calloc(fsize, sizeof(bool));

        while ((i<fsize) && ((c = getc(f)) != EOF)) {
            if (c == 'T') {
                (*training)[i] = true;
            } else if (c == 'G') {
                (*training)[i] = false;
                (*n_gen)++;
            } else {
                //ERROR BAD FILE
            }
            i++;
        } 
        fclose(f);
    }
    *n_inputs = log2(i);
}
