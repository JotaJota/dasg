#include "io.h"
#include "core.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>
#include <unistd.h>


bool test_solution(bool ** target_f, bool ** target_def, int * n_inputs, int * n_sol, bool ** sol, bool ** def, int * output_f) {
    /* This test is meant to be passed with the same file used for input
     * and will tell if the resultant network is correct according with the
     * samples provided, there will be another slightly different test which
     * will provide the generalization capabilities of the network thus the
     * fully defined specification will be needed.
     */

    int i = 0, j;
    int entries = (1<<*n_inputs);
    bool * result;
    bool ok = true;

    result = (bool *) calloc(entries, sizeof(bool));

    while ( ok && i<entries ) {
        result[i] = *output_f;
        if ((*target_def)[i]) {
            for (j=0; j<*n_sol; j++) {
                if (*output_f) 
                    result[i] = def[j][i]? sol[j][i] && result[i]:result[i];
                else 
                    result[i] = def[j][i]? sol[j][i] || result[i]:result[i];
            }
			ok = (result[i] == (*target_f)[i]);
        }
        i++;
    }

    free(result);
    result = NULL;
            
    return ok;
}

bool _eval(int input, int sol_n, int n, double ** w){
   int i;
   double tmp = 0.0;

   for (i = 0; i < n; i++) {
        if (input&(1<<i))
               tmp += w[sol_n][i];
   }
   
   return tmp >= w[sol_n][n];
}

bool test_weights(bool ** target_f, bool ** target_def, int * n_inputs, int * n_sol, double ** weights, int * output_f) {
    int i = 0, j;
    int entries = (1<<(*n_inputs));
    bool * result;
    bool ok = true;

    result = (bool *) calloc(entries, sizeof(bool));

    for (i = 0;ok && i<entries;i++) {
        result[i] = *output_f;
        if ((*target_def)[i]){
            for (j=0; j<*n_sol; j++) {
                if (*output_f) 
                    result[i] = _eval(i,j,*n_inputs,weights) && result[i];
                else 
                    result[i] = _eval(i,j,*n_inputs,weights) || result[i];
            }

			ok = (result[i] == (*target_f)[i]);
        }
    }

    free(result);
    result = NULL;
            
    return ok;
}

void test_gencap(bool ** target_f,int * n_inputs, int * n_undef, int * n_sol, double ** weights, int * output_f, double * gencap){
    /* This test will determine the best, worst and average generalization
     * capability of the network, the file used for this test is supposed to be
     * different from the one used to obtain the network, that is, the file used
     * here should not contain undefined values, while the one used at first
     * probably did. However, to obtain the percentage, the number of undefined
     * values involved on the early stage will be needed.
     */

    int i = 0, j;
    int entries = (1<<*n_inputs);
    bool * result;
    int errors = 0;

    result = (bool *) calloc(entries, sizeof(bool));

	FILE *f = fopen("result.txt", "w");
	FILE *g = fopen("target.txt", "w");

	if (f == NULL || g == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}

    for (i = 0; i < entries; i++) {
        result[i] = *output_f;
		if (*output_f) {
			for (j=0; j<*n_sol; j++) {
				result[i] = _eval(i,j,*n_inputs,weights) && result[i];
			}
		} else {
			for (j=0; j<*n_sol; j++) {
                result[i] = _eval(i,j,*n_inputs,weights) || result[i];
			}
		}

		fprintf(f,"%d",(int)result[i]);
		fprintf(g,"%d",(int)(*target_f)[i]);

        errors += (int) (result[i] != (*target_f)[i]);
    }


    printf("Estimated values: %d, Errors: %d\n", *n_undef, errors);
    *gencap = 100.0 - 100.0 * (double) errors / (double) *n_undef;

    free(result);
	fclose(f);
	fclose(g);
	}
