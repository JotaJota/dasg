#ifndef core_h
#define core_h

#include <stdbool.h>

typedef struct {
    bool value;
    bool defined;
}value_t;

typedef enum {
    OR,
    AND
}output_t;

void unate_dec (int * n_inputs, bool * vector, bool * defined, int * n_sol, output_t * of);
void populate_matrices (int * n_sol, int * entries, bool ** sol, bool ** def, double ** wei);

#endif
