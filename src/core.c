/**
    @file core.c
    @brief In this module the core processing of the algorithm is made.

    This module is the main functionality of the library.

    @author Jose J Guerrero
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "core.h"
#include "stack.h"
#include "simplex.h"

list working_set, solution_set, weights_set;

int get_mask (int pos){
/**
  @brief obtains the mask associated to a bit on the given position.

  Receives the position of the variable from right to left
  and returns the binary mask to switch that bit, which is
  2**pos 

  @param pos position of the bit
  @returns an integer such that the desired bit is selected using bitwise operators. 
*/

    return (1<<pos);
}

bool is_v_unate (value_t * v, int pos, int entries) {
/**
    @brief Tests the unateness of the function respect a given variable.
    
    Returns true if the function is unate for the variable in position pos
    (i. e. always positive(negative) for that variable) 
    
    @param v truth vector of the boolean function being analysed.
    @param pos position the variable being tested for unateness.
    @param entries number of entries of the truth vector.

    @returns true if variable in position pos is unate, false otherwise.
    */

    int i=0,mask,dir=0,tmp;
    bool unate = true;

    mask = get_mask(pos);

    while (i<entries && unate){
        if (v[i|mask].defined && v[i&(~mask)].defined) {
            tmp = v[i|mask].value - v[i&(~mask)].value;
            if (dir == 0) {  //function still not determined to be postive or negative 
                dir = tmp; //dir = f[xi=1] - f[xi=0]
            } else {
                unate = (tmp == dir) || (tmp == 0); //same direction or 0
            }
        }
        i++;
    }
    return (unate);
}

bool is_f_unate (value_t * v, int n_inputs, bool * unate) {
/* 
    @brief Tests the unateness of a boolean function.
    
    Returns the unateness of the whole function and an array
    containing the unateness of each variable individually 
    
    @param [in] v truth vector of the function.
    @param [in] n_inputs number of inputs of the function.
    @param [out] unate an array containing the unateness of each variable/input.
    @return true if all variables are (therefore the function is) unate, false otherwise.
*/

    int i, entries = (1<<n_inputs);
    bool is_unate = true;

    /* No cuento con las variables que han sido simplificadas
     * para el computo de unicidad, pero analizando la unicidad
     * de cada variable sí que analizo la tabla de verdad completa
     * incluyendo las variables simplificadas, ¿que problemas puede
     * traer esto? en principio creo que ninguno */

    for (i = 0; i < n_inputs; i++) {
        if (v[entries + i].defined) {
               unate[i] = is_v_unate(v,i,entries);
               is_unate = is_unate && unate[i];
        }
    }

    if (is_unate) 
        for (i = 0; i < n_inputs; i++){
            unate[i] = 0;
        }

    return (is_unate);
}

output_t sel_output(value_t * v, int entries) {
/** 
    @brief calculate which kind of gate will be applied at the output of the network.

    Returns what kind of function will be applied at the
    output to synthesize the original function. OR(AND)
    if more of the half of the outputs are true(false)
    
    @param v truth vector of the original function.
    @param entries number of entries of the function.
    @return a output_t type indicating the kind of function obtained.
    */

    unsigned int i, n_1s=0, n_0s=0;

    for (i = 0; i < (unsigned) entries; ++i)
    {
        if (v[i].defined && v[i].value) 
            n_1s++;
        else if (v[i].defined && !v[i].value)
            n_0s++;
    }

    return ((output_t) n_1s < n_0s);
}

int test_influence (int pos, value_t * v, int entries){
/** @brief tests the influence of a variable inside a given function.
 
    The influence of a variable is calculated counting the number of entries
    of the function in which a change only in the tested variable reflects a
    change in the output.

    @param pos position of the variable being tested.
    @param v truth vector of the function.
    @param entries number of entries of the truth vector.
    @return an integer indicating the influence of the variable.
    */

    int inf = 0;
    int i, mask;

    mask = get_mask(pos);

    for (i = 0; i < entries; i++) {
        inf += v[i].defined && v[i^mask].defined && (v[i].value != v[i^mask].value);
    }
    
    return (inf/2);
}

value_t * copy_vector(value_t * v, int n) {
    int i;
    value_t * c;

    c = (value_t *) malloc(n*sizeof(value_t));
    for (i=0;i<n;i++) {
        c[i].value = v[i].value;
        c[i].defined = v[i].defined;
    }
    return (c);
}

void print_vector (value_t * v, int entries) {
    int i;

    for (i=0;i<entries;i++){
        printf("%s, ", v[i].defined?(v[i].value?"1":"0"):"?");
    }
    printf("\n");
}

void print_set (list l, int n) {
    list tmp;
    value_t * vector;

    tmp = l;
    while (tmp){
        vector = (value_t *) tmp -> data;
        print_vector(vector,n);
        tmp = tmp -> next;
    }
}

        
bool * compact_v(value_t * v, int n, int of, int * presents) {
/** @brief compacts a vector eliminating superfluous variables
 
    Receives a vector in value_t format and the number 
    of inputs (variables present or not), with the variable 
    presence indicators and compacts it into a boolean 
    vector with the present variables only 

    @param v truth vector of the function.
    @param n number of inputs.
    @param of default value of the output in case of undefined entry.
    @param presents array representing the presence of the variables in the function.
    @return 
    */
    
    bool * read, * res;
    unsigned int i, j=0, k=0;
    unsigned int mask = 0;
    int entries = (1<<n);

    *presents = 0;
    read = malloc(entries * sizeof(bool)); // read[i] = 1 == i-th output read

    for (i = 0; i < entries; i++){
        read[i] = false;
    }


    for (i = 0; i < n; i++) {
        if (v[entries+i].defined) {
            (*presents)++;
            } else {
            mask = mask | (1<<i); // set bits in positions where variables are not present
        }
    }
            
    mask = ~mask;

    res = malloc((1<<(*presents)) * sizeof(bool));

    for (i = 0; i < entries; i++) {
        k = i&mask;
        if (!read[k]) {
            read[k] = true;
            res[j++] = v[k].defined?v[k].value:(int) of;
        }
    }    

    free(read);

    return (res);
}

void unate_dec (int * n_inputs, bool * vector, bool * defined, int * n_sol, output_t * of) {
/** @brief This function covers the main functionality of the algorithm. It
 * performs the unate decomposition of the first vector and does so recursively
 * until all obtained functions are linearly separable.
 *
 * This routine may well be defined as the core of the algorithm. It receives
 * the truth vector of the input function and tests for its unateness and
 * subsequently for its linear separability. Be the case that the function is
 * not unate nor ls, it's later splitted in two different functions based on
 * the variable with highest influence and repeats the operations on the
 * obtained set recursively until all the functions are ls thus being able to
 * be implemented with threshold logic.
 *
 * @param [in] n_inputs number of inputs of the input function.
 * @param [in] vector truth vector of the input function.
 * @param [in] defined array containing which variables are undefined.
 * @param [out] n_sol numbre of functions obtained after the processing is
 * finished.
 * @param [out] type of function used at the output of the network (AND/OR).
 */

    int max_inf,inf[*n_inputs],n_presents;
    unsigned long entries = (1<<(*n_inputs));
    size_t v_size;
    unsigned long i, j;
	int	mask;
    bool unate[*n_inputs], *v_compact, _unate = false, v_defined = false;
    value_t *tmp, *v, *v_a, *v_b;
    double *weights,*v_sol;
    
    srand(time(NULL));

    weights_set = NULL;
    solution_set = NULL;
    working_set = NULL;

    v_size = entries + *n_inputs;

    tmp = (value_t *) calloc (v_size ,sizeof(value_t));

    for (i = 0; i < entries; i++) {
        tmp[i].value = vector[i];
        tmp[i].defined = defined?defined[i]:true;
    }
    for (i = entries; i < v_size; i++) {
        tmp[i].defined = 1;
    }

    *of = sel_output(tmp,entries);
    insert(&working_set, (void *) tmp);
    tmp = NULL;

    while (working_set) {
    // EXTRAER F DEL CONJUNTO DE TRABAJO    
        v = (value_t *) extract(&working_set);


    // SIMPLIFICACION
        for (i = 0; i < (unsigned long) *n_inputs; i++) {
            inf[i] = test_influence(i,v,entries);
            // Simplificación por influencia = 0
            // si la influencia es cero se desactiva la variable
            if (!inf[i]) {
               mask = get_mask(i);
               for (j = 0; j < entries; j++) {
                   // simplificando en  el primer, segundo y cuarto 
                   // caso no es necesario modificar nada, 
                   // Tercer caso: F(xi=0)=* && F(xi=1) != *
                   // TODO: optimizar para recorrer solo la mitad del array
                    if (v[j|mask].defined && !(v[j&(~mask)].defined)) {
                        v[j&(~mask)].defined = true;
                        v[j&(~mask)].value = v[j|mask].value; 
                    }
               }
               v[entries + i].defined = false;
            }
            v_defined = v_defined || v[entries + i].defined;
        } 
        
        // Si en la simplificacion se anulan todas las variables se desecha la
        // funcion

        if (v_defined) {
        // ES F UNATE Y LS? 
            v_compact = compact_v(v,*n_inputs,*of,&n_presents);
            weights = (double *) calloc ((n_presents + 1), sizeof(double)); // weights and threshold
            _unate = is_f_unate(v,*n_inputs,unate);
            if ((_unate) && (ls_test(v_compact,n_presents,weights))) {
                    v_sol = (double *) calloc(((*n_inputs) + 1), sizeof(double));
                    j = 0;
                    for (i = 0; i < (unsigned long) *n_inputs; i++) {
                       if (v[entries+i].defined)
                           v_sol[i] = weights[j++];
                       else 
                           v_sol[i] = 0;
                    }

                    v_sol[*n_inputs] = weights[n_presents];

                    insert(&weights_set, (void *) v_sol);
                    insert(&solution_set, (void *) v);

                    (*n_sol)++;
            } else {
                //Cálculo de la variable con mayor influencia
                max_inf = -1;
                if (_unate) { 
                    //esta porcion de codigo hace que el codigo sea indeterminista
                    //elimino la aleatoriedad para comprobarlo.
                    do {
                        max_inf = rand() % *n_inputs;
                        } while ( !v[entries+max_inf].defined);

                } else {
                    //printf("Variante NO unate\n");
                    for (i = 0; i < (unsigned long)*n_inputs;i++) {
                        // No se tiene en cuenta las vbles, simplificadas ni las que 
                        // son unate para el cálculo de la max inf
                        if (v[entries + i].defined && inf[i] && !(unate[i])) {
                            if (max_inf == -1) {
                                max_inf = i;
                            } else {
                                max_inf = (inf[i] > inf[max_inf])?i:max_inf;
                            }
                        }
                    }
                    //printf("Max influence from var %d.\n", max_inf );
                }
        //DIVIDIR F EN DOS FUNCIONES MAS SIMPLES Y AÑADIRLAS 
        //AL CONJUNTO DE TRABAJO
                mask = get_mask(max_inf);
                v_a = (value_t *) calloc(v_size, sizeof(value_t));
                v_b = (value_t *) calloc(v_size, sizeof(value_t));

                for (i = 0; i < entries; i++) {
                    v_a[i].value = i&mask ? (int) *of : v[i].value;
                    v_a[i].defined = i&mask ? (v[i].defined && (v[i].value == (int) *of) ? true : false) : v[i].defined;
                    v_b[i].value = i&mask ? v[i].value : (int) *of;
                    v_b[i].defined = i&mask ? v[i].defined : (v[i].defined && (v[i].value == (int) *of) ? true : false);
                } 

                for (i = entries; i < v_size; i++) 
                    v_a[i].defined = v_b[i].defined = v[i].defined;

                insert(&working_set,(void *) copy_vector(v_a,v_size));
                insert(&working_set,(void *) copy_vector(v_b,v_size));

                free(v_a);
                free(v_b);
                free(v);
                v_a = NULL;
                v_b = NULL;
                v   = NULL;
            }
            free(v_compact);
            free(weights);
        }
        v_defined = false;
    }

    /*printf("\n\n\nProcessing over:\n");*/
    /*printf("Output function: %s\n", *of?"and":"or");*/
    /*printf("Number of solutions found: %d\n",*n_sol);*/

} 

void populate_matrices(int * n_sol, int * entries, bool ** sol, bool ** def, double ** wei) {
/** @brief Extracts the data obtained in unate_dec and presents it as a matrix.
 * 
 * This is a helper function that populates the data structures used to return
 * the solution to the problem. Returns two structures, one containing the
 * truth vector of each one of the functions composing the inner layer of the
 * network 
 **/

    int i,j;
    value_t * tmp;

    for (i=0;i<(*n_sol);i++) {
       sol[i] = (bool *) calloc(*entries, sizeof(bool));
       def[i] = (bool *) calloc(*entries, sizeof(bool));
       tmp = (value_t *) extract(&solution_set);
       wei[i] = (double *) extract(&weights_set);

       for (j=0;j<*entries;j++) {
           sol[i][j] = tmp[j].value; def[i][j] = tmp[j].defined;
       }
       free(tmp);
    }
}

