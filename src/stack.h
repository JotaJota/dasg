#ifndef stack_h
#define stack_h


typedef struct _element 
{
    struct _element *next;
    void *data;
}element;

typedef element * list;

list init();
void insert(list *l,void * data);
void * extract(list *l);
void destroy(list *l);

#endif
