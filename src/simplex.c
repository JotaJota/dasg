#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "lp_lib.h"

int int_hamming (int n, int n_inputs) {
    int i,h = 0;

    for (i = 0; i < n_inputs; i++) {
        h += n & (1<<i)?1:0;
    }
    return (h);
}
    

void to_simplex (int n, int n_inputs, bool * v, int * colno, double * sparserow) {
    /* Simplex method needs coefficients to be set in a specific
     * format, refer to simplex algorithm applied to the matter
     * of testing linear separability && lpsolve reference guide
     * in order to underestand the following transformations 
     */

    int i,j=0;

    for (i = 0; i < n_inputs; i++) {
        if (n & (1<<i)) {
            colno[j] = 2*i+1;  //lpsolve starts in 1, not zero
            colno[j+1] = 2*i+2;
            if (v[n]) {
                sparserow[j] = 1;
                sparserow[j+1] = -1;
            } else {
                sparserow[j] = -1;
                sparserow[j+1] = 1;
            }
            j += 2;
        }
    }
    // add the bias
    colno[j] = 2*n_inputs+1;  //lpsolve starts in 1, not zero
    colno[j+1] = 2*n_inputs+2;
    if (v[n]) {
        sparserow[j] = -1;
        sparserow[j+1] = 1;
    } else {
        sparserow[j] = 1;
        sparserow[j+1] = -1;
    }

} 

bool ls_test(bool * vector, int n_inputs, double * weights) {
    lprec *lp;
    int cols; //cols = non-zero values 
    double * sparserow = NULL, * sols = NULL;
    int * colno = NULL;
    int samples = (1<<n_inputs); 
    int i;
    bool ls = false;

    /* Creating the model */
    cols = n_inputs * 2 + 2;
    lp = make_lp(0,cols);

    if(lp == NULL) {
        fprintf(stderr, "Unable to create new LP model\n");
        return(1);
    }

	set_outputfile(lp, "lpsolve.log");

    /* Building the model */
    set_add_rowmode(lp, 1);

    for (i = 0; i < samples; i++) {
        cols = (int_hamming(i, n_inputs)*2 + 2); //twice the inputs set to 1 plus two more for the bias
        sparserow = (double *) realloc(sparserow,cols * sizeof(double));
        colno = (int *) realloc(colno,cols * sizeof(int));
        to_simplex(i,n_inputs,vector,colno,sparserow);
        add_constraintex(lp,cols,sparserow,colno,GE,1);
    }
    
    // the function to minimize will be the last one added
    // to the model
    set_add_rowmode(lp,0);
    set_obj_fnex(lp,cols,sparserow,colno);    
    // set minimize
    set_minim(lp);
    free(colno);
    free(sparserow);

    // trying to solve the model 
    if (solve(lp) == OPTIMAL) {
        ls = true;
        get_ptr_variables(lp,&sols);
        for (i = 0; i < n_inputs; i++) 
            weights[i] = sols[2*i] - sols[2*i+1];
        weights[n_inputs] = sols[2*n_inputs] - sols[2*n_inputs+1];
    }

    delete_lp(lp);

    return (ls);
}
