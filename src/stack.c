#include "stack.h"
#include <stdlib.h>
#include <stdio.h>


list init(){
   return NULL; 
}

void insert(list *l,void * data) {
    list tmp;

    tmp = (element *)malloc(sizeof(element));

    tmp -> data = data;
    tmp -> next = *l;
    *l = tmp;
}

void * extract(list *l) {
    void * data = NULL; 
    list tmp;

    if (*l) {
        tmp = *l;
        *l = (*l) -> next;
        data = tmp -> data;
        free(tmp);
    }

    return data;
}

void destroy(list *l) {
    void * tmp;

    tmp = extract(l);
    while (tmp){
        free(tmp);
        tmp = extract(l);
    }
}
        
