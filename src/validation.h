#ifndef validation_h
#define validation_h
#include <stdbool.h>
#include "core.h"

bool test_solution(bool ** target_f, bool ** target_def, int * n_inputs, int * n_sol, bool ** sol, bool ** def, int * output_f);
bool test_weights(bool ** target_f, bool ** target_def, int * n_inputs, int * n_sol, double ** weights, int * output_f);
void test_gencap(bool ** target_f, int * n_inputs, int * n_undef, int * n_sol, double ** weights, int * output_f, double * gencap);

#endif

